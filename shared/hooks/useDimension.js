import React, {useEffect, useState} from 'react';
import {Dimensions} from 'react-native';

const useDimension = () => {
  const [width, setWidth] = useState(() => Dimensions.get('window').width);
  const [height, setHeight] = useState(() => Dimensions.get('window').height);

  useEffect(() => {
    setWidth(Math.floor(Dimensions.get('window').width));
    setHeight(Math.floor(Dimensions.get('window').height));
  }, [Dimensions]);

  return {width, height};
};

export default useDimension;
