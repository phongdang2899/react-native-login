import {StyleSheet, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Controller} from 'react-hook-form';
import {
  FormControl,
  WarningOutlineIcon,
  Input,
  Center,
  IconButton,
} from 'native-base';
import Icon from 'react-native-vector-icons/Feather';

const InputForm = ({
  name,
  control,
  label,
  leftIcon = <></>,
  rightIcon = <></>,
  style = {},
  placeholder,
  isRequired,
  password,
  ...props
}) => {
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);

  const handlePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };
  console.log({password});
  return (
    <Controller
      control={control}
      render={({field: {onChange, value}, fieldState: {error}}) => (
        <Center mb="2">
          <FormControl isRequired={isRequired} isInvalid={!!error}>
            <FormControl.Label>{label}</FormControl.Label>
            <Input
              placeholder={placeholder}
              onChangeText={onChange}
              style={styles.input}
              px="3"
              type={isPasswordVisible ? 'text' : 'password'}
              InputRightElement={
                password ? (
                  <IconButton
                    icon={
                      <Icon
                        name={isPasswordVisible ? 'eye' : 'eye-off'}
                        size={20}
                      />
                    }
                    onPress={handlePasswordVisibility}
                  />
                ) : null
              }
            />
            <FormControl.ErrorMessage
              leftIcon={<WarningOutlineIcon size="xs" />}>
              {error?.message}
            </FormControl.ErrorMessage>
          </FormControl>
        </Center>
      )}
      name={name}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    backgroundColor: 'white',
  },
});

export default InputForm;
