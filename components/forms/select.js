import {StyleSheet, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Controller, useController} from 'react-hook-form';
import {
  Box,
  FormControl,
  WarningOutlineIcon,
  Center,
  Select,
  CheckIcon,
} from 'native-base';
import _ from 'lodash';

const SelectForm = ({
  name,
  control,
  label,
  style = {},
  placeholder,
  isRequired,
  options,
  ...props
}) => {
  return (
    <Controller
      control={control}
      render={({field: {onChange}, fieldState: {error}}) => (
        <Center mb="2">
          <FormControl isReadOnly isRequired={isRequired} isInvalid={!!error}>
            <FormControl.Label>{label}</FormControl.Label>
            <Select
              accessibilityLabel={placeholder}
              placeholder={placeholder}
              bgColor="white"
              _selectedItem={{
                bg: 'teal.600',
                endIcon: <CheckIcon size={5} />,
              }}
              px="3"
              onValueChange={onChange}>
              {_.map(options, (item, index) => {
                return (
                  <Select.Item
                    key={index}
                    label={item.label}
                    value={item.value}
                  />
                );
              })}
            </Select>
            <FormControl.ErrorMessage
              leftIcon={<WarningOutlineIcon size="xs" />}>
              {error?.message}
            </FormControl.ErrorMessage>
          </FormControl>
        </Center>
      )}
      name={name}
    />
  );
};

const styles = StyleSheet.create({});

export default SelectForm;
