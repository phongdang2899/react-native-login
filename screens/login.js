import {Pressable, StyleSheet} from 'react-native';
import React from 'react';
import InputForm from '../components/forms/input';
import {useForm} from 'react-hook-form';
import {Box, Button, ScrollView, Text, Link, View} from 'native-base';
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from 'yup';
import SelectForm from '../components/forms/select';

export default function Login({navigation}) {
  const schema = yup.object().shape({
    abc: yup.string().required('This field is required'),
    abcd: yup.string().required('This field is required'),
  });

  const fakeOption = [
    {
      label: 'UX Research1',
      value: 'ux',
    },
    {
      label: 'Web Development',
      value: 'web',
    },
    {
      label: 'Cross Platform Development',
      value: 'cross',
    },
    {
      label: 'UI Designing',
      value: 'ui',
    },
    {
      label: 'Backend Development',
      value: 'backend',
    },
  ];

  const {
    register,
    setValue,
    handleSubmit,
    control,
    reset,
    formState: {errors},
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = data => {
    console.log(data);
  };

  return (
    <ScrollView contentContainerStyle={styles.loginLayout}>
      <Box width="90%" py="5">
        <Text fontSize="2xl" mb="5" textAlign="center">
          Welcome to React App
        </Text>
        <InputForm
          name="email"
          placeholder="Email"
          control={control}
          label="Email"
          isRequired
        />
        <InputForm
          name="password"
          placeholder="Password"
          control={control}
          label="Pasword"
          password
          isRequired
        />
        <Pressable>
          <Text underline alignSelf="flex-end" color="blue.500">
            Forgot password?
          </Text>
        </Pressable>
        <Button colorScheme="info" mt="5" onPress={handleSubmit(onSubmit)}>
          Login
        </Button>
        <View style={styles.register}>
          <Text textAlign="center">Don't have an account? </Text>
          <Pressable onPress={() => console.log('Go to Register')}>
            <Text underline color="blue.500">
              Register
            </Text>
          </Pressable>
        </View>
      </Box>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  loginLayout: {
    alignItems: 'center',
    justifyContent: 'center',
    flexGrow: 1,
  },
  register: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
});
